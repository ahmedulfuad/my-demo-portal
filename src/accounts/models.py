from django.db import models
from django.contrib.auth.models import User   #from django.contrib.auth import get_user_model
from PIL import Image 	#pillow library

# Create your models here.
class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	image = models.ImageField(default='default.jpg', upload_to='profile')

	def __str__(self):
		return f'{self.user.username} Profile'	 #return '%s' % (self.user.username)
		#return self.user   #depend on purpose
		#return '%s' % (self.user.id)

	#override the save method
	def save(self, *args, **kwargs):
		#run the save method of parent class
		super().save(*args, **kwargs)	#positional argument and keyword argument

		img = Image.open(self.image.path)

		if img.height > 300 or img.width > 300:
			output_size = (300, 300)
			img.thumbnail(output_size)
			img.save(self.image.path) 	#save in same directory