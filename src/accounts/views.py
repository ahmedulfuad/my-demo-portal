from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages	  #flash message
from django.contrib.auth.decorators import login_required

from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm

# Create your views here.
def register(request):
	if request.method == 'POST':
		#form = UserCreationForm(request.POST)
		form = UserRegisterForm(request.POST)
		if form.is_valid():	  #form validation
			form.save()
			#the validated form data will be in this cleaned data dictionary and get the username
			username = form.cleaned_data.get('username')
			messages.success(request, f'Account created! Now try to log in')
			return redirect('login')
	else:
		#form = UserCreationForm()
		form = UserRegisterForm()
	return render(request, 'account/register.html', {'form': form})

@login_required(login_url="/login/")
def profile(request):
	if request.method == 'POST':
		u_form = UserUpdateForm(request.POST, instance=request.user)	#instance used to populate form with the current users information that means autofill
		p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

		if u_form.is_valid() and p_form.is_valid():
			u_form.save()
			p_form.save()
			messages.success(request, f'Your account has been updated!')
			return redirect('profile')
	else:
		u_form = UserUpdateForm(instance=request.user)
		p_form = ProfileUpdateForm(instance=request.user.profile)

	context = {
		'u_form': u_form,
		'p_form': p_form,
	}
	return render(request, 'account/profile.html', context)