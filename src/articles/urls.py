from django.urls import path
from . import views
from .views import PostListView, PostDetailView, PostCreateView, PostUpdateView, PostDeleteView, UserPostListView

app_name = 'articles'   #use in href 'app_name:path_name' in template
urlpatterns = [
	#path('', views.home, name='article-home'),
	path('', PostListView.as_view(), name='article-home'),
	path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
	path('post/new/', PostCreateView.as_view(), name='post-create'),
	path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
	path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
	path('about/', views.about, name='article-about'),
	path('user/<str:username>/', UserPostListView.as_view(), name='user-posts'),
]