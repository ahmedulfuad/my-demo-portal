from django.apps import AppConfig


class ArticlesConfig(AppConfig):	#ArticlesConfig class inherits from AppConfig class
    name = 'articles'
