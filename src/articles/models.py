from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.
# The following post model will be a class that inherits from django models.Model
class Post(models.Model):
	title = models.CharField(max_length=100)	 #attribute has some arguments
	content = models.TextField()
	date_posted = models.DateTimeField(default=timezone.now)
	author = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.title

	def snippet(self):	#custom method
		return '%s ...' % (self.content[:50])   #return self.content[:50] + '...'

		#(redirect)actually redirect to a specific route
		#(reverse)return full URL to that route as string
	def get_absolute_url(self):
		return reverse('post-detail', kwargs={'pk': self.pk})