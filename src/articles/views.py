from django.shortcuts import render, get_object_or_404
from .models import Post 	#import Post class from models file
#from django.http import HttpResponse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
# posts = [	#dummy information
# 	{
# 		'author': 'Fuad',
# 		'title': 'Post 1',
# 		'content': 'Content 1',
# 		'date_posted': 'September, 2019'
# 	},
# 	{
# 		'author': 'Fahim',
# 		'title': 'Post 2',
# 		'content': 'Content 2',
# 		'date_posted': 'October, 2019'
# 	},
# ]

# Create your views here.
def home(request):	#home funtion
	context = {
		'posts': Post.objects.all(),
	}
	return render(request, 'article/home.html', context)	  #three arguments- request object, template, context

class PostListView(ListView):   #home list view #class plv inherits from lv
	model = Post
	# <app>/<model>_<viewtype>.html -> article/post_list.html
	template_name = 'article/home.html'
	context_object_name = 'posts'
	ordering = ['-date_posted']
	paginate_by = 5

class UserPostListView(ListView):   #home list view #class plv inherits from lv
	model = Post
	template_name = 'article/user_posts.html'
	context_object_name = 'posts'
	paginate_by = 5

	def get_queryset(self):
		user = get_object_or_404(User, username=self.kwargs.get('username'))	#if user exists, go to blogs when not exist then return 404
		return Post.objects.filter(author=user).order_by('-date_posted')	#override filter method

class PostDetailView(DetailView):
	model = Post
	template_name = 'article/post_detail.html'

class PostCreateView(LoginRequiredMixin, CreateView):
	model = Post
	fields = ['title', 'content']
	template_name = 'article/post_form.html'

	#override form valid method
	def form_valid(self, form):
		form.instance.author = self.request.user   #take instance set author equals to current logged in user
		return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
	model = Post
	fields = ['title', 'content']
	template_name = 'article/post_form.html'

	#override form valid method
	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)

	def test_func(self):	#use for UserPassesTestMixin to add condition
		post = self.get_object()
		if self.request.user == post.author:
			return True
		return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Post
	template_name = 'article/post_delete.html'
	success_url = '/article/'

	def test_func(self):	#use for UserPassesTestMixin to add condition
		post = self.get_object()
		if self.request.user == post.author:
			return True
		return False

def about(request):
	return render(request, 'article/about.html', {'title': 'About'})   #change the title name by context